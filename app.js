/**
 * Created by bingocn on 7/06/15.
 */

var express = require('express');
var path = require('path');

var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.set('port', (process.env.PORT || 8001));

/*
Define the routes
 */
app.use('/', express.static(path.join(__dirname, 'dist')));
app.use('/getclassinfo', require('./routes').getClassInfo);
app.use('/viewclass', require('./routes').viewClass);
app.use('/deleteclass', require('./routes').deleteClass);
app.use('/getstudentlist', require('./routes').getStudentList);
app.use('/getclasslist', require('./routes').getClassList);
app.use('/addClass', require('./routes').addClass);
app.use('/addstudent', require('./routes').addStudent);
app.use('/studentsinclass', require('./routes').getStudentsInClass);
app.use('/deletestudentsinclass', require('./routes').deleteStudentInClass);

app.listen(app.get('port'), function() {
    console.log('Server started: http://localhost:' + app.get('port') + '/');
});
