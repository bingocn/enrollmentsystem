/**
 * Created by bingocn on 7/06/15.
 */
//var config = require('../config/config');
var db     = require('../modules/mysqlHandler');
var httpObj= require('../modules/httpHandler');

module.exports = {
    getClassInfo: function(req, res){
        db.queryClassInfo(res);
    },

    viewClass: function(req, res){
        db.queryClassDetails(req, res);
    },

    deleteClass: function(req, res){
        db.deleteClass(req, res);
    },

    getClassList: function(req, res){
        db.queryClassList(res);
    },

    getStudentList: function(req, res){
        httpObj.getStudentList(res);
    },

    addClass: function(req, res){
        db.addClass(req, res);
    },

    addStudent: function(req, res){
        db.addStudent(req, res);
    },

    getStudentsInClass: function(req, res){
        db.getStudentsInClass(req, res);
    },

    deleteStudentInClass: function(req, res){
        db.deleteStudentInClass(req, res);
    }
};
