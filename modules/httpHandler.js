/**
 * Created by bingocn on 10/06/15.
 */

var config = require('../config/config');

/*
 Construct response data
 */
function responseJsonInfo(res, data) {
    res.writeHead(200, {"Content-Type": "application/json"});
    var json = JSON.stringify(data);
    res.end(json);
}

/*
 Get a Json file from URL
 */
function getRemoteJson(url, res) {
    var request = require('request');
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            responseJsonInfo(res, JSON.parse(body).Students);
        }
        else {
            console.log("Failed to getRemoteJson");
            responseJsonInfo(res, []);
        }
    });
}

module.exports = {
    /*
    Query stutent list
     */
    getStudentList: function(res) {
        return getRemoteJson(config.studentUrl, res);
    }
};
