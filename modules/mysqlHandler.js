/**
 * Created by bingocn on 7/06/15.
 */
var config = require('../config/config');

/*
Query from MySQL.
Param:
    statement:  SQL statement
    post:       used for insert a JSON object
    res:        object to save response data
    callback:   used to construct response
 */
function queryMysql(statement, post, res, callback) {
    var mysql      = require('mysql');
    var connection = mysql.createConnection({
        host     : config.dbIP,
        user     : config.dbUsername,
        password : config.dbPassword,
        database : config.database,
        multipleStatements: true
    });

    connection.connect(function(err){
        if(err) {
            console.log("Error connecting database ... \n\n");
            responseError(res, 'Error connecting database.');
        }
    });

    if(post == null) {
        var query = connection.query(statement, function(err, rows, fields) {
            console.log(query.sql);
            connection.end();
            if (!err) {
                callback(res, rows);
            }
            else {
                console.log('Error while performing Query.');
                responseError(res, 'Error while performing Query.');
            }
        });
    }
    else {
        var query = connection.query(statement, post, function(err, rows, fields) {
            console.log(query.sql);
            connection.end();
            if (!err) {
                callback(res, rows);
            }
            else {
                console.log('Error while performing Query.');
                responseError(res, 'Error while performing Query.');
            }
        });
    }
}

function responseError(res, data) {
    res.writeHead(500, {"Content-Type": "application/text"});
    res.end(data);
}

function cbResponseJsonInfo(res, data) {
    res.writeHead(200, {"Content-Type": "application/json"});
    var json = JSON.stringify(data);
    res.end(json);
}

module.exports = {
    /*
    Query all records from class_info.
     */
    queryClassInfo: function(res) {
        return queryMysql('select *,date_format(startingDate,"%Y/%m/%d") as formattedDate from class_info', null, res, cbResponseJsonInfo);
    },

    /*
    (NOT USED) Query related records from enroll_info and student_info
     */
    queryClassDetails: function(req, res) {
        return queryMysql('select classID,studentName,enrollDate from enroll_info inner join student_info on enroll_info.studentID=student_info.studentID where classID="'+req.query.classid+'"', null, res, cbResponseJsonInfo);
    },

    /*
    Delete a class in class_info and related records in enroll_info
     */
    deleteClass: function(req, res) {
        var classid = req.body.classid;
        return queryMysql('delete from class_info where classID="'+classid+'";' +
            'delete from enroll_info where classID="'+classid+'";',
            null,
            res,
            cbResponseJsonInfo);
    },

    /*
    Query class list. Used for adding student
     */
    queryClassList: function(res) {
        return queryMysql('select classID,className from class_info', null, res, cbResponseJsonInfo);
    },

    /*
    Insert a new class
     */
    addClass: function(req, res) {
        return queryMysql('insert into class_info set ?', req.body, res, cbResponseJsonInfo);
    },

    /*
    Insert a new student
     */
    addStudent: function(req, res) {
        return queryMysql('insert into enroll_info set ?', req.body, res, cbResponseJsonInfo);
    },

    /*
    Query all students in a class
     */
    getStudentsInClass: function(req, res) {
        return queryMysql('select classID,studentID,date_format(enrollDate,"%Y/%m/%d") as formattedDate from enroll_info where classID="'+req.body.classID+'"', null, res, cbResponseJsonInfo);
    },

    /*
    Delete a student in a class
     */
    deleteStudentInClass: function(req, res) {
        var classid = req.body.classID;
        var studentid = req.body.studentID;
        return queryMysql('delete from enroll_info where classID="'+classid+'" and studentID="' + studentid+'"',
            null,
            res,
            cbResponseJsonInfo);
    }
};