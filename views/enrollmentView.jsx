/**
 * Created by bingocn on 8/06/15.
 */
var React           = require('react');
var Griddle         = require('griddle-react');
var Calendar        = require('react-input-calendar');
var PubSub          = require('pubsub-js');

var ButtonGroup     = require('react-bootstrap').ButtonGroup;
var DropdownButton  = require('react-bootstrap').DropdownButton;
var MenuItem        = require('react-bootstrap').MenuItem;
var Button          = require('react-bootstrap').Button;
var ListGroup       = require('react-bootstrap').ListGroup;
var ListGroupItem   = require('react-bootstrap').ListGroupItem;
var Row             = require('react-bootstrap').Row;
var Grid            = require('react-bootstrap').Grid;
var Col             = require('react-bootstrap').Col;
var Input           = require('react-bootstrap').Input;
var ButtonInput     = require('react-bootstrap').ButtonInput;
var Panel           = require('react-bootstrap').Panel;
var Label           = require('react-bootstrap').Label;
var ButtonToolbar   = require('react-bootstrap').ButtonToolbar;

/*
Used to control the page in right side
 */
var EnumRightPage = {
    "Empty"     : 0,
    "AddClass"  : 1,
    "AddStudent": 2,
    "ViewClass" : 3
};

/*
Component of Class table (left side)
 */
var ClassTable = React.createClass({
    loadClassInfoFromServer: function() {
        $.ajax({
            url: 'getclassinfo',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('getclassinfo', status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        // data: store the class info
        return {data: []};
    },
    componentDidMount: function() {
        this.loadClassInfoFromServer();
    },
    deleteClass: function(classid) {
        var sendData = {};
        sendData.classid = classid;
        $.ajax({
            url: 'deleteclass',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(sendData),
            dataType: 'json',
            cache: false,
            success: function(data) {
                alert("Delete class success");
                PubSub.publish("refreshclass");
            }.bind(this),
            error: function(xhr, status, err) {
                alert("Failed to delete class "+classid);
            }.bind(this)
        }).bind(this);
    },
    render: function() {
        // Construct Edit and Delete buttons
        var LinkComponent = React.createClass({
            clickViewClass: function(classID, className){
                PubSub.publish("viewstudents", {classID: classID, className: className});
            },
            clickDeleteClass: function(classID){
                ClassTable.prototype.deleteClass(classID);
            },
            render: function(){
                return (
                    <ButtonToolbar>
                        <Button bsStyle="info" bsSize="small" onClick={this.clickViewClass.bind(null, this.props.rowData.classID, this.props.rowData.className)}>View</Button>
                        <Button bsStyle="danger" bsSize="small" onClick={this.clickDeleteClass.bind(null, this.props.rowData.classID)}>Delete</Button>
                    </ButtonToolbar>
                );
            }
        });
        var metadata = [
            {
                "columnName": "classID",
                "displayName": "Class ID"
            },
            {
                "columnName": "className",
                "displayName": "Class Name"
            },
            {
                "columnName": "formattedDate",
                "displayName": "Starting Date"
            },
            {
                "columnName": "maxStudents",
                "displayName": "# of Students"
            },
            {
                "columnName": "actions",
                "displayName": "Actions",
                "customComponent": LinkComponent
            }
        ];
        // Construct Griddle table
        var renderData = <Griddle results={this.state.data} columnMetadata={metadata} tableClassName="table" columns={["classID", "className", "formattedDate", "maxStudents", "actions"]}/>;
        return (
            <div>{renderData}</div>
        );
    }
});

/*
Left side page
 */
var ClassView = React.createClass({
    updateData: function() {
        this.refs.refClassTable.loadClassInfoFromServer();
    },
    render: function() {
        return (
            <div>
                <h3>
                    Classes
                </h3>
                <ClassTable ref={'refClassTable'} callback={this.updateData} />
            </div>
        )
    }
});

/*
 Component of Calendar
  */
var StartCalendar = React.createClass({
    render: function() {
        var today = new Date();
        return (
            <Calendar
                format="DD/MM/YYYY"
                date={today}
                computableFormat="DD/MM/YYYY"
                onChange={this.props.callback}
            />
        )
    }
});

/*
 Component of add class
 */
var AddClassSheet = React.createClass({
    getInitialState() {
        return {
            classID: '',
            className: '',
            maxNum: 20,
            startingDate: new Date()
        };
    },
    classIDChange: function(e) {
        this.setState({classID: e.target.value});
    },
    classNameChange: function(e) {
        this.setState({className: e.target.value});
    },
    maxNumChange: function(e) {
        this.setState({maxNum: e.target.value});
    },
    startingDateChange: function(e) {
        this.setState({startingDate: ''});
    },
    clickAddClass: function() {
        var sendData = {};
        sendData.classid = this.state.classID;
        sendData.classname = this.state.className;
        sendData.maxstudents = this.state.maxNum;
        sendData.startingDate = new Date().toISOString().slice(0, 10);
        $.ajax({
            url: 'addclass',
            type: 'POST',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(sendData),
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({classID: '', className: ''});
                {this.props.callback()};
                alert("Add class success")
                document.getElementById('idClassID').value='';
                document.getElementById('idClassName').value='';
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to add class', status, err.toString());
                alert("Failed to add class")
            }.bind(this)
        });
    },
    render: function() {
        return (
            <form>
                <Input type='text' id='idClassID' label='Class ID' placeholder='Enter Class ID' onChange={this.classIDChange} />
                <Input type='text' id='idClassName' label='Class Name' placeholder='Enter Class Name' onChange={this.classNameChange} />
                <Input type='select' label='Maximum number of enrollment' placeholder='20' onChange={this.maxNumChange} >
                    <option value='20'>20</option>
                    <option value='25'>25</option>
                    <option value='30'>30</option>
                    <option value='35'>35</option>
                    <option value='40'>40</option>
                    <option value='45'>45</option>
                    <option value='50'>50</option>
                </Input>
                <h5>Starting Date</h5>
                <StartCalendar callback={this.startingDateChange} />
                <br/>
                <Button bsStyle="primary" bsSize="small" onClick={this.clickAddClass}>New Class</Button>
            </form>
        )
    }
});

/*
Component of add student
 */
var AddStudentSheet = React.createClass({
    getInitialState: function() {
        return {
            studentlist     : [],
            classlist       : [],
            selectedStudent : '',
            selectedClass   : ''
        };
    },
    loadStudentListFromServer: function() {
        $.ajax({
            url: 'getstudentlist',
            dataType: 'json',
            cache: false,
            success: function(data) {
                if(data instanceof Array) {
                    this.setState({studentlist: data, selectedStudent: data[0].id});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to get student list', status, err.toString());
            }.bind(this)
        });
    },
    loadClassListFromServer: function() {
        $.ajax({
            url: 'getclasslist',
            dataType: 'json',
            cache: false,
            success: function(data) {
                if(data instanceof Array) {
                    this.setState({classlist: data, selectedClass: data[0].classID});
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to get class list', status, err.toString());
            }.bind(this)
        });
    },
    componentDidMount: function() {
        this.loadStudentListFromServer();
        this.loadClassListFromServer();
    },
    clickAddStudent: function() {
        var sendData = {};
        sendData.classID = this.state.selectedClass;
        sendData.studentID = this.state.selectedStudent;
        sendData.enrollDate = new Date().toISOString().slice(0, 10);
        $.ajax({
            url: 'addstudent',
            type: 'POST',
            async: false,
            contentType: 'application/json',
            data: JSON.stringify(sendData),
            dataType: 'json',
            cache: false,
            success: function(data) {
                alert("Add student success")
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to add student', status, err.toString());
                alert("Failed to add student")
            }.bind(this)
        });
    },
    studentChange: function(e) {
        this.setState({selectedStudent: e.target.value});
    },
    classChange: function(e) {
        this.setState({selectedClass: e.target.value});
    },
    render: function() {
        var studentList = this.state.studentlist.map(function (item) {
            return (
                <option value={item.id}>{item.id}: {item.name}</option>
            );
        });
        var classList = this.state.classlist.map(function (item) {
            return (
                <option value={item.classID}>{item.classID}: {item.className}</option>
            );
        });
        return (
            <form>
                <Input type='select' label='Student Name:' placeholder='Loading' onChange={this.studentChange}>
                    {studentList}
                </Input>
                <Input type='select' label='Class Name:' placeholder='Loading' onChange={this.classChange}>
                    {classList}
                </Input>
                <br/>
                <Button bsStyle="primary" bsSize="small" onClick={this.clickAddStudent}>Add Student</Button>
            </form>
        )
    }
});

/*
Component of view students in a class
 */
var StudentTable = React.createClass({
    getInitialState: function() {
        return {
            classdata   : [],
            studentdata : [],
            mergeddata  : []};
    },
    loadData: function() {
        this.setState({classdata:[], studentdata:[], mergeddata:[]});
        this.loadStudentsInClassFromServer();
        this.loadStudentListFromServer();
    },
    componentDidMount: function() {
        var fireRefreshStudents = function(msg) {
            this.loadData();
        }.bind(this);
        PubSub.subscribe("refreshstudents", fireRefreshStudents);
        this.loadData();
    },
    componentWillUnmount: function() {
        PubSub.unsubscribe("refreshstudents");
    },
    mergeTable: function() {
        console.log("classdata=");
        console.log(this.state.classdata);
        console.log("studentdata=");
        console.log(this.state.studentdata);
        var result = [], classdata = this.state.classdata, studentdata = this.state.studentdata;
        for(var i=0; i<classdata.length; i++) {
            for(var j=0; j<studentdata.length; j++) {
                if(classdata[i].studentID == studentdata[j].id) {
                    var student = {};
                    student.classID = classdata[i].classID;
                    student.studentID = classdata[i].studentID;
                    student.studentName = studentdata[j].name;
                    student.enrollmentDate = classdata[i].formattedDate;
                    result.push(student);
                    break;
                }
            }
        }
        this.setState({mergeddata: result});
        console.log("result = ");
        console.log(result);
    },
    loadStudentsInClassFromServer: function() {
        var sendData = {};
        sendData.classID = this.props.classID;
        $.ajax({
            url: 'studentsinclass',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(sendData),
            dataType: 'json',
            cache: false,
            success: function(data) {
                if(data instanceof Array) {
                    console.log("Get students in class ok");
                    console.log(data);
                    this.setState({classdata: data});
                    if(this.state.classdata.length>0 && this.state.studentdata.length>0) {
                        this.mergeTable();
                    };
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to get students in class', status, err.toString());
                alert("Failed to get students in class")
            }.bind(this)
        });
    },
    loadStudentListFromServer: function() {
        $.ajax({
            url: 'getstudentlist',
            dataType: 'json',
            cache: false,
            success: function(data) {
                if(data instanceof Array) {
                    console.log("Get students ok");
                    console.log(data);
                    this.setState({studentdata: data});
                    if(this.state.classdata.length>0 && this.state.studentdata.length>0) {
                        this.mergeTable();
                    };
                }
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to get student list', status, err.toString());
            }.bind(this)
        });
    },
    deleteStudent: function(classID, studentID) {
        var sendData = {};
        sendData.classID = classID;
        sendData.studentID = studentID;
        $.ajax({
            url: 'deletestudentsinclass',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(sendData),
            dataType: 'json',
            cache: false,
            success: function(data) {
                alert("Delete student success");
                PubSub.publish("refreshstudents");
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('Failed to delete student in class', status, err.toString());
                alert("Failed to delete student in class")
            }.bind(this)
        });
    },
    render: function() {
        var LinkComponent = React.createClass({
            clickDeleteStudent: function(classID, studentID){
                StudentTable.prototype.deleteStudent(classID, studentID);
            },
            render: function(){
                var studentID = this.props.rowData.studentID;
                var classID = this.props.rowData.classID;
                return (
                    <Button bsStyle="danger" bsSize="xsmall" onClick={this.clickDeleteStudent.bind(null, classID, studentID)}>Remove</Button>
                );
            }
        });
        var metadata = [
            {
                "columnName": "studentID",
                "displayName": "Student ID"
            },
            {
                "columnName": "studentName",
                "displayName": "Student Name"
            },
            {
                "columnName": "enrollmentData",
                "displayName": "Enrollment Date"
            },
            {
                "columnName": "classID",
                "displayName": "Class ID"
            },
            {
                "columnName": "actions",
                "displayName": "Actions",
                "customComponent": LinkComponent
            }
        ];
        var renderData = <Griddle results={this.state.mergeddata} resultsPerPage={10} showFilter={true} columnMetadata={metadata} tableClassName="table" columns={["studentID", "studentName", "enrollmentDate", "actions"]}/>;
        return (
            <div>{renderData}</div>
        );
    }
});

/*
Component in right side page
    - Add class
    - Add student
    - View students in a class
 */
var RightSideView = React.createClass({
    render: function() {
        if(this.props.page == EnumRightPage.AddClass) {
            return (
                <div>
                    <br />
                    <br />
                    <Panel>
                        <h4>
                        New Class
                        </h4>
                        <AddClassSheet callback={this.props.cbAddedClass}/>
                    </Panel>
                </div>
            )
        }
        if(this.props.page == EnumRightPage.AddStudent) {
            return (
                <div>
                    <br />
                    <br />
                    <Panel>
                        <h4>
                        Add Student
                        </h4>
                        <AddStudentSheet />
                    </Panel>
                </div>
            )
        }
        if(this.props.page == EnumRightPage.ViewClass) {
            return (
                <div>
                    <br />
                    <h4>
                        {this.props.classID} : {this.props.className}
                    </h4>
                    <StudentTable classID={this.props.classID} />
                </div>
            )
        }
        return (
            <br/>
        )
    }
});

/*
Component of buttons
    - Add class
    - Add student
 */
var Buttons = React.createClass({
    render: function() {
        return (
            <ButtonToolbar>
                <Button bsStyle="primary" onClick={this.props.cbAddClass}>New Class</Button>
                <Button bsStyle="primary" onClick={this.props.cbAddStudent}>Add Student</Button>
            </ButtonToolbar>
        );
    }
});

/*
Main layout
 */
var EnrollmentView = React.createClass({
    getInitialState: function() {
        return {
            rightPage : EnumRightPage.Empty,
            classID   : '',
            className : ''
        };
    },
    componentDidMount: function() {
        var fireViewStudent = function(msg, obj) {
            console.log("PubSub.subscribe ID="+obj.classID+",Name="+obj.className);
            this.viewStudentsInClass(obj.classID, obj.className);
        }.bind(this);
        PubSub.subscribe("viewstudents", fireViewStudent);
        var fireRefreshClass = function(msg) {
            this.refreshClass();
        }.bind(this);
        PubSub.subscribe("refreshclass", fireRefreshClass);
    },
    componentWillUnmount: function() {
        PubSub.unsubscribe("viewstudents");
        PubSub.unsubscribe("refreshclass");
    },
    addClass: function() {
        this.setState({rightPage: EnumRightPage.AddClass});
    },
    addStudent: function() {
        this.setState({rightPage: EnumRightPage.AddStudent});
    },
    refreshClass: function() {
        this.refs.refClassView.updateData();
    },
    viewStudentsInClass: function(classID, className) {
        console.log("viewStudentsInClass ID="+classID+",Name="+className);
        this.setState({rightPage: EnumRightPage.ViewClass, classID: classID, className: className});
        PubSub.publish("refreshstudents");
    },
    render: function() {
        return (
            <Grid>
                <Row>
                    <Col xs={10} md={7}>
                        <ClassView ref={'refClassView'} />
                        <br/>
                        <br/>
                        <Buttons cbAddClass={this.addClass} cbAddStudent={this.addStudent} />
                    </Col>
                    <Col xs={8} md={5}>
                        <RightSideView page={this.state.rightPage} cbAddedClass={this.refreshClass} classID={this.state.classID} className={this.state.className} />
                    </Col>
                </Row>
            </Grid>
        )
    }
});

React.render(
    <EnrollmentView />,
    document.getElementById('content')
);
