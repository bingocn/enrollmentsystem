/**
 * Created by bingocn on 9/06/15.
 */

var path = require('path');

module.exports = {
    entry: path.resolve(__dirname, '../views/enrollmentView.jsx'),
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {
                test: /views\/.+.jsx$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            }
        ]
    }
};
