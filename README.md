# Enrollment System

Enrollment System is a demo by using Node.js and React.js. It completed the features:

* Class management (add/view/delete)
* Add & Remove a student in a class
* View students in a class

### Used technologies and modules

* React
* Node.js
* Webpack
* React-bootstrap
* Express
* Request
* react-input-calendar
* Griddle
* pubsub-js

### Installation

Students info is from a http link and classes info is saved in MySQL database. So we should have a MySQL server and initialise a database. The config file of MySQL is in config/config.js. You should change the IP address and password.

Initialise a database and a user:
```sh
CREATE USER bingocn_enroll IDENTIFIED BY 'ChangeToYourPassword';
CREATE DATABASE bingocn_enrollment;
GRANT ALL ON bingocn_enrollment.* TO bingocn_enroll;
flush privileges;

CREATE TABLE `bingocn_enrollment`.`class_info` ( `classID` TEXT NOT NULL , `className` TEXT NOT NULL , `startingDate` DATE NOT NULL , `maxStudents` INT NOT NULL , PRIMARY KEY (`classID`(64)) ) ENGINE = MyISAM;

CREATE TABLE `bingocn_enrollment`.`student_info` ( `studentID` TEXT NOT NULL , `studentName` TEXT NOT NULL , PRIMARY KEY (`studentID`(64)) ) ENGINE = MyISAM;

CREATE TABLE `bingocn_enrollment`.`enroll_info` ( `classID` TEXT NOT NULL , `studentID` TEXT NOT NULL , `enrollDate` DATE NOT NULL ) ENGINE = MyISAM;
```

Install the dependent libraries:
```sh
$ npm install
```

### Run service

```sh
$ node app.js
```
Then you can visit http://localhost:8001 in your browser. (localhost should be server's IP address)

### Live Demo
http://oz.touchwow.com:8001